package com.barry.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailEvent {

    private String content;
    private String orderId;
    private String orderStatus;
    private LocalDate localDate;
}
