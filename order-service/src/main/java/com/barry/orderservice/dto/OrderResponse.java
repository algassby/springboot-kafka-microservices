package com.barry.orderservice.dto;

public record OrderResponse(String message) {
}
