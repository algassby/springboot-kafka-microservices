package com.barry.orderservice.service;

import com.barry.orderservice.domain.OrderEntity;
import com.barry.orderservice.dto.EmailEvent;
import com.barry.orderservice.dto.Order;
import com.barry.orderservice.dto.OrderEvent;
import com.barry.orderservice.repository.OrderRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderProducer {

    private final OrderRepository orderRepository;
    private final NewTopic topic;
    private final KafkaTemplate<String, OrderEvent> orderEventKafkaTemplate;
    private final ObjectMapper objectMapper;

    public OrderEntity sendMessage(OrderEvent orderEvent){

        log.info(String.format("Order event => %s", orderEvent.toString()));
        Message<OrderEvent> eventMessage =
                MessageBuilder
                        .withPayload(orderEvent)
                        .setHeader(KafkaHeaders.TOPIC, topic.name())
                        .build();
        orderEventKafkaTemplate.send(eventMessage);

        OrderEntity orderEntity = new OrderEntity();
        Order order = orderEvent.getOrder();
        orderEntity.setOrderId(order.getOrderId());
        orderEntity.setName(order.getName());
        orderEntity.setQty(order.getQty());
        orderEntity.setPrice(order.getPrice());
        orderEntity.setStatus(orderEvent.getStatus());
        return orderRepository.save(orderEntity);
    }

    @KafkaListener(topics = "${spring.kafka.topic2.name}", groupId = "order_service")
    public OrderEntity consumer(EmailEvent emailEvent) throws JsonProcessingException {

        //EmailEvent eventEmailMapper = objectMapper.readValue(emailEvent, EmailEvent.class);
        log.info(String.format("Order event => %s", emailEvent.toString()));


        OrderEntity orderEntity = orderRepository.findById(emailEvent.getOrderId()).get();
        if(!emailEvent.getOrderStatus().equals("VALID")){
           return orderEntity;
        }
        orderEntity.setStatus(emailEvent.getOrderStatus());
        return orderRepository.save(orderEntity);
    }
}
