package com.barry.emailservice.service;

import com.barry.emailservice.dto.EmailEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailProducer {

    private final NewTopic topic;
    private final KafkaTemplate<String, EmailEvent> eventKafkaTemplate;


    public void publishEvent(EmailEvent emailEvent){
        log.info(String.format("Order event => %s", emailEvent.toString()));
        Message<EmailEvent> eventMessage =
                MessageBuilder.withPayload(emailEvent)
                        .setHeader(KafkaHeaders.TOPIC, topic.name())
                                .build();
        eventKafkaTemplate.send(eventMessage);
    }


}
