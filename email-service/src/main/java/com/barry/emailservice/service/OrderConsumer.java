package com.barry.emailservice.service;

import com.barry.emailservice.dto.EmailEvent;
import com.barry.emailservice.dto.OrderEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderConsumer {

    private final EmailProducer emailProducer;
    private final ObjectMapper objectMapper;

    @KafkaListener(
            topics = "${spring.kafka.topic1.name}"
            ,groupId = "${spring.kafka.consumer.group-id}"
    )
    public void consume(String event) throws JsonProcessingException {
        OrderEvent orderEvent = objectMapper.readValue(event, OrderEvent.class);
        log.info(String.format("Order event received in stock service => %s", orderEvent.toString()));
        EmailEvent emailEvent = new EmailEvent();
        emailEvent.setOrderId(orderEvent.getOrder().getOrderId());
        emailEvent.setOrderStatus("VALID");
        emailEvent.setContent("successfuly");
        emailEvent.setLocalDate(LocalDate.now());

        emailProducer.publishEvent(emailEvent);

    }
}